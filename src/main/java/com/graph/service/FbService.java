package com.graph.service;

import com.graph.model.ResponseTO;
import facebook4j.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FbService {

    private Facebook facebook = new FacebookFactory().getInstance();

    public List<ResponseTO> search(String parameters) throws FacebookException {
        List<ResponseTO> results = new ArrayList<>();
        ResponseList<Page> searchResults = facebook.searchPages(parameters, new Reading().fields("location, name"));
        for (Page page : searchResults) {
            ResponseTO pageResult = new ResponseTO();
            pageResult.setName(page.getName());
            if (page.getLocation() != null
                    && page.getLocation().getLatitude() != null
                    && page.getLocation().getLongitude() != null) {
                pageResult.setLongitude(page.getLocation().getLongitude().floatValue());
                pageResult.setLatitude(page.getLocation().getLatitude().floatValue());
            }
            results.add(pageResult);
        }
        return results;
    }
}
