package com.graph.web;

import com.graph.model.ResponseTO;
import com.graph.service.FbService;
import facebook4j.FacebookException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class CodingTaskController {

    @Autowired
    private FbService fbService;

    @GetMapping(
            value = "/{country}/{city}/{name}",
            produces = "application/json; charset=UTF-8")
    @ResponseBody
    public List<ResponseTO> fbSearch(@PathVariable("country") String country,
                                     @PathVariable("city") String city,
                                     @PathVariable("name") String name) throws FacebookException {
        return fbService.search(country + " " + city + " " + name);
    }

    @GetMapping(
            value = "/{country}/{name}",
            produces = "application/json; charset=UTF-8")
    @ResponseBody
    public List<ResponseTO> fbSearchTwo(@PathVariable("country") String country,
                                     @PathVariable("name") String name) throws FacebookException {
        return fbService.search(country + " " + name);
    }

    @GetMapping(
            value = "/{name}",
            produces = "application/json; charset=UTF-8")
    @ResponseBody
    public List<ResponseTO> fbSearchOne(@PathVariable("name") String name) throws FacebookException {
        return fbService.search(name);
    }
}
