package com.graph.controller;

import com.graph.service.FbService;
import com.graph.web.CodingTaskController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CodingTaskController.class, secure = false)
public class CodingTaskControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FbService fbService;

    @Test
    public void testEndpoint() throws Exception {
        this.mockMvc.perform(get("/poland/poznan/egnyte").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }
}
